import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Cart from '../app/components/e-commerce/Cart'
import ProductDetails from '../app/components/e-commerce/ProductDetails'
// import ProductList from '../app/components/e-commerce/ProductList'
// import ProductList from '../app/components/e-commerce/ProductList'
import Test from '../app/components/e-commerce/Test'
// import ProductList from '../app/components/e-commerce/ProductList'
// import Test from '../app/components/e-commerce/Test'
// import ProductList from '../app/components/e-commerce/ProductList'

// import ProductDetails from '../app/components/e-commerce/ProductDetails'

export default function routing () {
  return (
    <div>
      <BrowserRouter>
        <Switch>
            <Route exact path="/" component={Test} />
            <Route path="/product" component={ProductDetails}/>
            <Route path="/cart" component={Cart}/>
        </Switch>
      </BrowserRouter>
    </div>
  )
}
