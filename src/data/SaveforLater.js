const SaveforLater = [
  {
    productId: 1234,
    productName: '(Renewed) Samsung Galaxy A21s (Blue, 4GB RAM, 64GB Storage) with No Cost EMI/Additional Exchange Offers',
    images: {
      imgUrl: 'https://m.media-amazon.com/images/I/71-Su4Wr0HL._AC_AA180_.jpg',
      altText: ''
    },
    stockQuantity: 4,
    quantity: 2,
    price: 25000,
    numberOfItems: 1
  }
]
export default SaveforLater
