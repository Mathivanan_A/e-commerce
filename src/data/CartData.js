const CartDetails = [
  {
    productId: 1234,
    productName: '(Renewed) Samsung Galaxy A21s (Blue, 4GB RAM, 64GB Storage) with No Cost EMI/Additional Exchange Offers',
    images: {
      imgUrl: 'https://m.media-amazon.com/images/I/71-Su4Wr0HL._AC_AA180_.jpg',
      altText: ''
    },
    stockQuantity: 4,
    quantity: 2,
    price: 2500,
    numberOfItems: 1
  },
  {
    productId: 1234,
    productName: 'Intex 6.2 kg Semi-Automatic Top Loading Washing Machine (WMS62TL, White and Maroon)',
    images: {
      imgUrl: 'https://m.media-amazon.com/images/I/91mRu98fVjL._AC_AA180_.jpg',
      altText: ''
    },
    stockQuantity: 4,
    quantity: 2,
    price: 6500,
    numberOfItems: 1
  }
]
export default CartDetails
