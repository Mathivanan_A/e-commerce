const ProductDetailsData = [
  {
    productId: '1234',
    productName: '(Renewed) Samsung Galaxy A21s (Blue, 4GB RAM, 64GB Storage) with No Cost EMI/Additional Exchange Offers',
    images: [
      {
        imgUrl: 'https://images-na.ssl-images-amazon.com/images/I/71pGhztrr9L._SL1500_.jpg'
      },
      {
        imgUrl: 'https://images-na.ssl-images-amazon.com/images/I/811RH5e-snL._SL1500_.jpg'
      },
      {
        imgUrl: 'https://images-na.ssl-images-amazon.com/images/I/71Bxwrp7S5L._SL1500_.jpg'
      },
      {
        imgUrl: 'https://images-na.ssl-images-amazon.com/images/I/71pGhztrr9L._SL1500_.jpg'
      },
      {
        imgUrl: 'https://images-na.ssl-images-amazon.com/images/I/811RH5e-snL._SL1500_.jpg'
      }
    ],
    rating: 61143,
    numberOfUserRated: 3,
    MRP: '28000.00',
    price: '22999.00',
    discountPercentage: '21%',
    discountPrice: '6,000.00',
    currencySymbol: 'INR',
    shortDescripton: 'Inclusive oF all Taxes',
    stockAvailability: 'In Stock',
    deliverBy: 'Amazon',
    getItBy: 'April 21, Monday',
    emiAvailability: true,
    emiDescription: 'EMI starts at ₹965. No Cost EMI available',
    offers: [
      {
        offerName: 'Bank Offer',
        offerDescription: 'Get 7.5% up to Rs. 1250 Instant Discount on Yes Bank Credit Card EMI transactions',
        offerDetails: ''
      },
      {
        offerName: 'Cash back',
        offerDescription: 'Get Flat Rs 100 back with Amazon Pay Later. Offer applicable on 1st sign-up.',
        offerDetails: ''
      },
      {
        offerName: 'Exchange',
        offerDescription: 'Up to ₹ 19,250.00 off on Exchange',
        offerDetails: ''
      },
      {
        offerName: 'No cast EMI',
        offerDescription: 'Avail No Cost EMI on select cards for orders above ₹3000',
        offerDetails: ''
      }
    ],
    services: [
      {
        image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRuFey1lRdEA7HhnbEBdfcKd7QqWbgeUSYW5g&usqp=CAU',
        name: 'Pay on Delivery',
        description: 'Cash on delivery'
      },
      {
        image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ-moAj0zFgxIkIgIliZSrBPyzhWitOuTx2jQ&usqp=CAU',
        name: '7 days replacement',
        description: '7 days replacement'
      },
      {
        image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRIbz8Y5j_3jOlZ7WqgZilBke195op6yLf96w&usqp=CAU',
        name: '7 days replacement',
        description: 'Amazon Delivered'
      },
      {
        image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS18Zj2UH-CKjBR9v2z1RM-BzODux8WnAdsjA&usqp=CAU',
        name: '7 days replacement',
        description: '1 Year warranty'
      }
    ],
    sellerDetails: {
      sellerId: 'S234',
      sellerName: 'STPL exclusive Online'
    },
    color: 'Migrage Blue',
    variantImages: [
      {
        imgUrl: 'https://m.media-amazon.com/images/I/41mPjjqqNnL._SS36_.jpg',
        description: ''
      },
      {
        imgUrl: 'https://m.media-amazon.com/images/I/41mPjjqqNnL._SS36_.jpg',
        description: ''
      }
    ],
    specification: [
      {
        spec1: 'Quad camera setup - 64MP (F1.8) main camera + 12MP (F2.2) ultra wide camera + 5MP (F2.4) depth camera + 5MP (F2.4) macro camera | 32MP (F2.2) front camera',
        spec2: '16.4 centimeters (6.5-inch) super Amoled - Infinity-O display, FHD+ capacitive multi-touch touchscreen with 1080 x 2400 pixels resolution, 407 ppi pixel density and Contrast Ratio: 78960:1',
        spec3: 'Memory, Storage & SIM: 6GB RAM, 128GB internal memory expandable up to 512GB | Dual SIM (nano+nano) dual-standby (4G+4G)',
        spec4: 'Android v10.0 operating system with 1.7GHz+2.3GHz Exynos 9611 octa core processor',
        spec5: '6000mAH lithium-ion battery with 5x fast charge | 25W Type-C fast charger in the box',
        spec6: '1 year manufacturer warranty for device and 6 months manufacturer warranty for in-box accessories including batteries from the date of purchase',
        spec7: 'Box also includes: Travel adapter, USB Type-C to Type-C Cable, ejection pin and user manual',
        spec8: 'Fast face unlock and fingerprint sensor | Dual SIM (nano+nano) with dual standby and dual VoLTE, Dedicated Sim slot'
      }
    ],
    imgCHeck: 'https://m.media-amazon.com/images/I/41mPjjqqNnL._SS36_.jpg'

  }
]
export default ProductDetailsData
