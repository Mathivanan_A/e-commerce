import './App.css'
import 'font-awesome/css/font-awesome.min.css?raw'
import 'bootstrap/dist/css/bootstrap.min.css'
import React from 'react'
import Routing from './navigation/Routing.js'
// import Header from './app/components/common/Header'
function App () {
  return (
    <div className="App">
      {/* <Header/> */}
      <Routing/>
    </div>
  )
}

export default App
