import { Layout, Menu } from 'antd'
import React from 'react'
import ProductList from '../e-commerce/ProductList'
import 'antd/dist/antd.css'
const { Content, Footer, Sider } = Layout
export default function SIdebar () {
  return (
            <Layout>
                <Sider className="sidebar"
                    breakpoint="lg"
                    collapsedWidth="0"
                    onBreakpoint={broken => {
                    }}
                    onCollapse={(collapsed, type) => {
                    }}
                >
                    <div className="logo" />
                </Sider>
                <Layout>
                    <Content>
                        <div className="site-layout-background" style={{ padding: 20, minHeight: 360, marginLeft: -10 }}>
                        <ProductList/>
        </div>
                    </Content>
                    <Footer style={{ textAlign: 'center' }}>Ant Design ©2018 Created by Ant UED</Footer>
                </Layout>
            </Layout>
  )
}
