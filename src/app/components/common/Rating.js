import React from 'react'
import ReactStars from 'react-rating-stars-component'

export default function Rating () {
  return (
        <div>
            <ReactStars
                count={5}
                onChange=''
                value={4}
                size={24}
                isHalf={true}
                emptyIcon={<i className="far fa-star"></i>}
                halfIcon={<i className="fa fa-star-half-alt"></i>}
                fullIcon={<i className="fa fa-star"></i>}
                activeColor="#ffd700"
            />
        </div>
  )
}
