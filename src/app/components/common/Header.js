import React, { useState } from 'react'
import { setUserInput } from './Util'
// import { userInput } from './Util'
export default function Header () {
  // eslint-disable-next-line
   const [inputString, setString] = useState('')
  return (
        <>
            <div>
                <div className="header mainHeader">
                    <div className="">
                        <nav className="navbar navBgColor nav-left">
                            <ul className="nav">
                                <li className="nav-item">
                                    <a className="nav-link active" href="#">
                                        <img src="\images\TAvertical.png" width="" height="45" alt=""></img>
                                    </a>
                                </li>
                                <li className="nav-item addressNav">
                                    <i className="fa fa-map-marker mr-2" aria-hidden="true"></i>
                                    <small className="mr-5 pr-5">Hello,</small><br></br>
                                select your Address
                            </li>
                                <li className="nav-item">
                                    <form>
                                        <input className="form-control-lg ml-5 mt-2 searchInput" placeholder="Search..." type="text" onChange={ e => setUserInput(e.target.value)}></input>
                                    </form>
                                    <button className="btn btn-primary">Search</button>
                                </li>
                                <li className="nav-item rtlSymbol">
                                    <div className="rtlSymbol">
                                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAARMAAAC3CAMAAAAGjUrGAAAAllBMVEX/mTMSiAf/////kAsAfQAAAIgAAH4AAIUAAIIAAHbm5vLy8vgAAHLj4/CBgbgAAHmkpMopKZPFxd66utf6+v7s7PWamsVAQJra2uyIiLsjI5FTU6L09Pqrq89KSp6RkcAdHY+xsdJpaa7MzOJAQJt7e7U3N5lHR6BqaqxZWaRgYKifn8jBwd2VlcXJyeK1tdQQEIwuLpSv9P++AAAEhElEQVR4nO3bbXOiSBSG4Uzv9Asgr4KCgEZRdNSNyf//c3saktSsx5qt/UKnKs9VFYOGD+0doFHx6QkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA4H/6C+49CbiHJhyacGjCoQmHJtxXaFLMN7s+XaX9bjMvXA9GfIEm/iZVxmgtpdTaGLXa+K6H5LhJVytjY0itlNJ2QRpVd24H5bSJXysbwZwP8ToIg3V8OBubSNVOtxWXTS52yzBmIRpxDUQpgistLWhHoioXh+Ny2KQ29Nyjtg/EsK/M7U0ngj6JqJWp3Q3MWZPmmTaHY5jb5a29aT+X8vBIf3tuXA3NVZNG0rZAe8iLvZOFdFPRT5jZuy+0V9E2JF1FcdWEthLTRoXI7ebRbOjmTD8bm6HNRRG1FOXZ0dgcNakpyXVYSuxZ2pp++vffRTI8fqUojo4pbposjDRbMUaxT/xMc++K5ubz+31KIra0zsLJ6Jw0CRXNOHT8+NveSWjvSejJa0pFm0g7bCb0l4pmHxW6GJ6TJmut1wu7z+zstEPH0jIVucpFWtKxlx7Jd3RTLOxqLobnokmnpBpPVBs6ORFbeuJekXt54VEumo2DfpxxfFrPxWm+iyZHrat8mIRFIWnvMHORJoEXJKmYG0ohxxfHL3ml9dHB+Bw0oaMJHWDfDxxhXIqbEtHS9/xlJNRNlPFwEGnpMEOHWRdHFAdNKppQ7L/fr+25STe7CnnK4tAL4+wkxXVmd5ft8CrwSGtW0w/QQZNXaS7BsJTIG73OmbXdzFelKZU/69oZPXST40vA4GLk6/QDnL6JnYg/loOlnotklh2iKuuzKjpks0TM5TL4WMHJdDx9k63Rp6bbZuPccot1d/X8tGwPbZn63rV7jW/DX5ps2zUnbbaTj3D6JhHNOvQr3Bx3SUkLp1nkr8Jov9xHIf2e7eixsj0dN+W4cjT5CKdv8kvLeDzr8BcrL40yP3oLj/aNJHMM3yI/q96858W4x3Sx1L8mH+H0TVZSrz/fBSiSZayqMjLSMlFZmXh/+XzzvqFT2dXkI5y+iZTqM0meB35YZu1eD030vs3K0A/y/GOFRkk5+QidNCnK+XZz3qdSeUT1h70cLaPeDA/pVV9H7a3zv00TbCd3Hh1Pqj8cT9LJR4h5h/sq5ycVnZ9U/Pyk+h7nJziP5fB65wG8Lubw/gmH99kewPuxHN63f+C/P9/Jv9vnO/gc8BF8XvxATdsAriu486/rTwJcf2I1GtcpMbie7ZHazj5R0hd31z223/a6R4HrYx8qfruOugiL366jdvpVBFxvz+F7GZzzJmL8/s4+XaX7E76/81WhCYcmHJpwaMKhCff0E+49/YB7aMKhCYcmHJpwaMKhCYcmHJpwaMKhCYcmHJpwaMKhCYcmHJpwaMKhCYcmHJpwaMKhCYcmHJpwaMKhCYcmHJpwaMKhCYcmHJpwaMKhCYcmHJpwaMKhCYcmHJpwaMKhCYcmHJpwaMKhCYcmHJpwaMKhCYcmHJpwaMKhCYcmHJpwaMKhCYcm3D9wkyKTM7CL2wAAAABJRU5ErkJggg==" height="40" width="50"></img>
                                    </div>
                                </li>
                                <li className="nav-item accountsNav">
                                    {/* <i className="fa fa-map-marker mr-2" aria-hidden="true"></i> */}
                                    <small className="mr-5">Hello,mathi</small><br></br>
                                Accounts & Lists
                        </li>
                                <li className="nav-item accountsNav">
                                    {/* <i className="fa fa-map-marker mr-2" aria-hidden="true"></i> */}
                                    <small className>Returns,</small><br></br>
                                & Orders
                        </li>
                                <li className="nav-item accountsNav">
                                    <i className="fa fa-shopping-cart cartIcon mr-3" aria-hidden="true"></i><br></br>
                                    <small className>Cart,</small><br></br>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </>
  )
}
