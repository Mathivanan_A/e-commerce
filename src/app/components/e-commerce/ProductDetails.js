import React, { useState } from 'react'
import ProductDetailsData from '../../../data/ProductDetailsData'
import Rating from '../common/Rating'
import { SideBySideMagnifier } from 'react-image-magnifiers'
import { Link } from 'react-router-dom'

export default function ProductDetails () {
  const [previewImg, setPreviewImg] = useState('https://images-na.ssl-images-amazon.com/images/I/71pGhztrr9L._SL1500_.jpg')
  // const showMainImg = (imgurl) => {
  //   return (
  //     <div>
  //       <img src={imgurl} alt="" height="550" width="250" className="mainImgPosition"></img>
  //     </div>
  //   )
  // }
  const previewImage = (previewImg) => {
    return (
      <SideBySideMagnifier className="mainImgPosition"
  imageSrc = {previewImg}
  imageAlt = "Example"
  largeImageSrc ={previewImg}
  alwaysInPlace="true"
        />
    )
  }
  return (

    <div className="row">
      <div className="col-md-4">
        {ProductDetailsData.map((productData) => (
          <div key={productData} className>
            {productData.images.map((mainImg) => (
              <div key={mainImg} className="text-left mt-4 ml-3">
                <button className="btn" onClick={() => setPreviewImg(mainImg.imgUrl)}><img src={mainImg.imgUrl} height="60" width="25"></img></button>
              </div>
            ))}
            {/* {showMainImg(previewImg)} */}
            {previewImage(previewImg)}
            <br></br>
            {/* <img src="https://images-na.ssl-images-amazon.com/images/I/811RH5e-snL._SL1500_.jpg" alt="" height="400" width="200" className="mainImgPosition"></img> */}
            <div>
            </div>
            {/* <div className="identity">
              <h1>haai</h1>
            </div> */}
          </div>
        ))}
      </div>
      <div className="col-md-5  mt-5">
        {ProductDetailsData.map((productData) => (
          <div key={productData}>
            <h5 className="text-left">{productData.productName}</h5>
            <a className="text-left" href="https://samsung.in" style={{ textDecoration: 'none' }}><p>View in Samsung store</p></a>
            <div className="row">
              <div className="col-sm-3 ">
                <Rating />
              </div>
              <div className="col-sm-1 mt-2 proDetailsRatingSym">
                {productData.rating}&nbsp;ratings&nbsp;|&nbsp;1000+&nbsp;Answered&nbsp;Questions
                </div>
            </div>
            <div className="text-left">
              <img src="images\TAvertical.png" alt="" height="35" width="150"></img>
              <span className="ml-1">for &quot;Samsung M51s&quot;</span>
            </div>
            <hr></hr>
            <div className="text-left">
              M.R.P.&nbsp;<strike><i className="fa fa-inr ml-1 mr-2" aria-hidden="true"></i>{productData.MRP}</strike><br></br>
              &nbsp;&nbsp;&nbsp;<span>Deal Price:</span>&nbsp;<i className="fa fa-inr ml-1 mr-2" aria-hidden="true"></i><span className="textColorRed">{productData.price}</span><br></br>
                You save:&nbsp;<i className="fa fa-inr ml-1 mr-2" aria-hidden="true"></i><span className="textColorRed">{productData.discountPrice}</span>
              <p className="ml-5 pl-1">{productData.shortDescripton}</p>
              <a href="/gp/help/customer/display.html?ie=UTF8&pop-up=1&nodeId=200904360" style={{ textDecoration: 'none' }}>FREE Delivery:</a>&nbsp;<b>{productData.getItBy}</b> <a style={{ textDecoration: 'none' }} href="/gp/help/customer/display.html/ref=ftinfo_dp_?ie=UTF8&pop-up=1&nodeId=200534000">Details</a><br></br>
              {productData.emiDescription}&nbsp;&nbsp;<a><span style={{ color: 'skyblue' }}>EMI options&nbsp;<i className="fa fa-angle-down" aria-hidden="true"></i></span></a>
            </div>
            <div>
              <div className=" ml-0 card text-left">
                <div className="card-title ml-4 mt-3 mb-0" >
                  <b><span className="textColorRed">Save Extra</span> With 5 offers</b>
                </div>
                <hr></hr>
                {productData.offers.map((offers) => (
                  <div key={offers}>
                    <b><span className="textColorRed ml-3">{offers.offerName}:</span></b>
                    {offers.offerDescription}
                    <hr></hr>
                  </div>
                ))}
              </div>
            </div>
            <div className="row">
              {productData.services.map((services) => (
                <div key={services}>
                  <div className="col mt-5 ml-3">
                    {<img src={services.image} alt="" height="60" width="70" className="servicesLogo mr-3 "></img>}
                  </div>
                </div>
              ))}
            </div>
            <div className="row">
              {productData.services.map((services) => (
                <div key={services}>
                  <div className="ml-3 wordWrap">
                    {services.description}
                  </div>
                </div>
              ))}
            </div>
            <hr></hr>
            <div className="text-left">
              <b className="text-left"> <span style={{ color: 'green', margin: 'auto 86% auto 0%' }}>{productData.stockAvailability}</span></b>
              <p className="text-left mt-3 ml-1"><b>Sold by</b>&nbsp;<a href="" style={{ textDecoration: 'none' }}>{productData.sellerDetails.sellerName}</a>&nbsp;and&nbsp;<a href="" style={{ textDecoration: 'none' }}><span>Fullfilled by Amazon</span></a></p>
              <p className="text-left ml-1"><b>color:</b>{productData.color}</p>
            </div>
            <div className="text-left row">
              {productData.variantImages.map((variantImg) => (
                <div key={variantImg} className="mt-2 ml-3">
                  <button className="btn"><img src={variantImg.imgUrl} height="30" width="30"></img></button>
                </div>
              ))}
            </div>
            <div className="text-left mt-2">
              <span>Style Name : <b>8 GB RAM</b></span>
            </div>
            <div className="text-left mt-2">
              <input type="button" className="" value="6 Gb RAM"></input>&nbsp;
             <input type="button" value="8 Gb RAM"></input>
            </div>
            <div className="text-left mt-3">
              {productData.specification.map((specs) => (
                <ul key={specs}>
                  <li>{specs.spec1}</li>
                  <li>{specs.spec2}</li>
                  <li>{specs.spec3}</li>
                  <li>{specs.spec4}</li>
                  <li>{specs.spec5}</li>
                  <li>{specs.spec6}</li>
                  <li>{specs.spec7}</li>
                  <li>{specs.spec8}</li>
                </ul>
              ))}
            </div>
          </div>
        ))}
      </div>
      <div className="col-md-3 mt-5 ">
        <div className="card">
          <div classNam="card-header" style={{ backgroundColor: '#f4f5db' }}>
            <input className="form-group mr-2" type="radio" /><b>With Exchange</b><br></br>
            <span className="textColorRed">Upto &nbsp;<i className="fa fa-inr ml-1 mr-2" aria-hidden="true"></i><span className="textColorRed">16.500.00 off</span> </span>
          </div>
          <div className="card-body">
            <input className="mr-2" type="radio" /><b>Without Exchange</b><br></br>
            <span className="textColorRed"><i className="fa fa-inr ml-1 mr-2" aria-hidden="true"></i>16.500.00</span><strike><i className="fa fa-inr ml-1 mr-2" aria-hidden="true"></i>22.999.00</strike><br></br>
           <Link to="cart" style={{ textDecoration: 'none', color: 'white' }}><input className="form-control addToCartBtn text-center ml-2 mt-3" value="Add to Cart" type="button"></input></Link>
            <input className="form-control addToCartBtn text-center ml-2 mt-3" value="Buy Now" type="button"></input><br></br>
            <div className="text-left ml-3">
              <i className="fa fa-lock mr-2" aria-hidden="true"></i>Secure Transaction<br></br>
              <input type="checkbox" className="mr-1" /> Add gift options<br></br>
              <i className="fa fa-map-marker mr-2 mt-4" aria-hidden="true"></i>Select delivery information
                </div>
          </div>
        </div>
        <input value="Add to wishlist" className="form-control mt-3 ml-5" style={{ width: '85%', background: 'grey' }}></input>
      </div>
    </div>
  )
}
