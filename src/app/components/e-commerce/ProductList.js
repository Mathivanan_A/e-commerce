
import axios from 'axios'
import React, { useEffect, useState } from 'react'
// import productData from '../../../data/ProductData'
import Rating from '../common/Rating'
import { getUserInput } from '../common/Util'
import Sider from './Sider'
// import Header from '../common/Header'

export default function ProductList (props) {
  const openNewWindow = () => {
    window.open('product')
  }
  // eslint-disable-next-line no-console
  console.log('hai' + JSON.stringify({ props }))
  // eslint-disable-next-line no-console
  // eslint-disable-next-line react/prop-types
  const [productData, setProducts] = useState([])
  // const [actualData, setActualData] = useState([])

  useEffect(() => {
    axios.post('http://localhost:9200/product_list/_search',
      { query: { multi_match: { query: getUserInput(), fields: 'productName' } } }
    )
      .then((response) => {
        // eslint-disable-next-line no-console
        // console.log('responseeeeeeeee' + JSON.stringify(response.data))
        if (response.data !== undefined) {
          setProducts(response.data.hits.hits)
        } else {
          setProducts([])
        }
        // console.log(responseData)
      })
  }, [])
  return (
    <div className="row">
      <div className="">
        <Sider name='mathi'/>
      </div>
      <div className="col-md-10">
        {/* {console.log(JSON.Stringyfy({ mathi: 'mathivanan' }))} */}
        {productData.map((products) =>
          <div key={products.productId} className="card">
            <div className="row" key={products.productId}>
              <div className="card-img col-md-3 mt-5 mb-5">
                <img src={products._source.imageUrl}></img>
              </div>
              <div className="card-body col-md-9 mt-5  productDescriptions text-left">
                <h5 onClick={() => openNewWindow()}>{products._source.productName}</h5><br></br>
                <Rating onClick={() => { openNewWindow() }} value={products.rating}/>
                <p className="rating">{products._source.rating}</p>
                <p>{products.shortDescription}</p>
                <i className="fa fa-inr" aria-hidden="true"> </i>  {products.price}
                <strike className="MRP ml-3">{products._source.MRP}<span>{products._source.currencySymbol}</span></strike>save {products.discountPrice}<br></br>
                {products._source.discountPercentage}
                           Save extra with No Cost EMI<br>
                </br>
                            Get it by <b>{products._source.getItBy}<br></br></b>
                            Free Delivery By Amazon
                        </div>
            </div>

          </div>
        )}
      </div>
    </div>
  )
}
