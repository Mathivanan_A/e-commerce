import React from 'react'
import Sider from './Sider'

export default function Layout (props) {
  // eslint-disable-next-line react/prop-types
  const { children } = props
  return (
        <div>
            <Sider>
                {children}
            </Sider>
        </div>
  )
}
