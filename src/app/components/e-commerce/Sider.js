import React from 'react'
export default function Sider (props) {
  return (
        <div className="row">
            <div className="col-sm-12 mt-4 text-left">
                <b><h6 className="text-left ml-4">Departments</h6></b>
                <p className="text-left ml-4">Smartphones & Basic Mobiles</p>
                <img className="ml-4" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQIAAADECAMAAABDV99/AAABUFBMVEX////y8vL09PTx8fHw8PDz8/MAAADZ2dn6+vri4uLt7e3/rgCioqKampr39/evr6/S0tL/xwD/vAD/wgD/tAHTvoT1zkrIyMj/uQC/dQC/v7/49e/Xv6T/0jnqmAD/sgHRqAd/f3/IrV3/1k27gCJmZmaQkJCdnZ11dXWHh4dHR0e6urovLy+pqalRUVFbW1sAZcBubm5Ric07OzvV4vIpKSkQEBAAXb7s8/q70espeMcfHx9OTk45OTnf6fWlv+PHoVnL3PAAacKvyOeVtd8AXr4AVbsYGBjh0b3z6+Dp3c3HqHuyhT7HhwBxndVakNAAQbWDqNk1fcnbt4Gzgi/Fn2mudAC8gwDPuJbaowDLlAC5kFTvzYvLrYLstQC7iTD/0gD/4o62j1zz4sixhETYlQC7ijfLlirVkgDSxJ27mjO9cQDMsmTThgCrnXbquxgmoES7AAARd0lEQVR4nO2d+1/iSpqHC4y5Nbl42U0vsO2kp5MQIgTw0gEV5KIttg0H1m61Xe05PeM5fXZmdv//3/atSlBAtGOfCDqnno+BVFIJqW/eur2VlAhRKBQKhUKhUCgUCoUyc2I5bdaXcCeCabiGLTz2z7gs++i/8YNILCE3YZe1kYnud4osK0Z3tihR2E3WtW3XmbDPYI3ofohXn6gR8Bssm/RXEwlYSVoJHskJw7U1PrnFZnMJGWTSXVOFGKrliJKRUXkl4ybwIYLpFrF2CUsULbIFORbsNmUUswwDTqUk4ANplsVoZI1PFA1JRsmEFYcolgAbEg4SbNeQZmUjHDu40zzLFhHKsKyMVnHOeCewm5vwzaBtklMshEw/z2xu4U9IsUOCOkIsCzl9h5zGYLNwOCPgQ9k1fAiIl2VZpOMzixt48wafY1kNJVnWRALLSiobnHAmQCqkQIJNFnI+XCivweVwjhVLQBpyCVxYZJw1XJiRNcjTWcXCysXgCCGBd0Bm2lrNk9PA7s3VVTnLbnNwnqSIk0k+iARFdkdVN9jcXLAliy9ALLJbqppTZySBwrLKuARwUzIk3/oWsgU3E1sLEYPBa3DEKusiSKIgivh+ghUMTggSxBBOtMMwLsjr+skUfAmg2GXmDTCcPLsNtrG5Kdtw9gz7TuFnknyMg28H4VoCmVh+nhtIQDIIZJMMlkAMJFiDRFsssXYiwXW5CRLApxrsMvEPxDNs3hdXDDZnEBgPw+YNlsuCTBzeZs9KBAEbI2FIAqRA5oWbOZAAPmWc58ckgAyt27ati5MkYIt4l4bkTVbZwEdgCRiIiDcnsZnYrJRj7XcsaC3am2yUtc/D2PH152WeXIQvAbGOgX3vQPGFjT43LgFs4/yT3JKAuSnddHaHnDPICIHNQU7aZDUOPjYGx72bToJvk8S2mc3CPYbC2s5AiE9um7kiTh5cZrEoQ3GxDTcLrGJMAqhQN3XJzgq3JYCKgS1KppvEpQgxL18CG2LC5hxukcEv4XIUNDGKVm4nqFFmgeNnT9uv8qBy450gv+KyEidaJ+EkqRQFLAGkYANnb/Ud2SMMF4euL4GYJbtwo2GV1Iu+BLI7qGAhB66S2hL25YMiZWbwWkLKaXEQw1REXoVL4nKmRKoowXHwN6dIDoODqsqjuKpCI0ZVcR6Qk5KkCIMQgVODyk1LmDmyVfC3BF9qzvSrPxW3FskZ4QosM/dEW88UCoVCoVAolO8wu679I5I+HAm206O7P44G/0sePfjgMS5p2hyPJvL9m5EbffBpRKH2yenowUePdVlT5ODsc3somDq5HDGD88sRhT7+93lq+OCLi/vNQAjtPOe2d6ywcSPmbwtLw4k8vfzLlyEzOPz8l89DZpA6+flw2AyOUqnj0dMxpO8bdJ81vzs9CXW8e7y1oySvj2GnVeS0D9MXX88WFj5/vUgftlHq8OD0668LC5fn79MHbR4H3x8tLCz98vH04DCV+uvBxe7x2c/o/Ohj+jCF2gfpj0cXoNnx19ODGzuCDrdgsv7YjMi6AoN74hMw3NGwPBjSSrIad9P9fmT485PLywXC4uWnU3T66exyiQSXLk/O0eFQ8PNxu/3p2+XiwsLPcGD74NMBOj8+CMyjffjp/fB51UFqbHZoq6PrIMy8BEah5VBybS3h6xS3MqaAmBxrJIhhOMMHPT4fzxaWfD7jAiD9OQgtnOG8cfgmCC5dHqdwSXi5tLSIJUDnuAD4ep0j3o8owF17DncyiBtkA5s1DNZGAnZAZbbktXdb22TMUt5i9VWWU9fYnSw5LveO46ZZ874/WyR88IvA9IkfPPPT1H7zkgRfHpEiMPVmefElSMAf+UXg14vgJBcj54ScXIyRta0dWM+SWlTAgzYWK4pYAn0VoXzgcJTweOuGCxkhGNKwcFHgr2vwxUmPlfYBX5dfAsuD23h6hYMvfwluw+GHRQgtfgiyeurTy2WQ4GBw+/9GtqfG6kWOU4LicDMvxDU85oYLOQ7roF1LsB1IYGDHqb2J5ga5J87ExWIwDi0pXGZyURIh58uYlfMgeLpCwr8G7Z/DKxK8CjJ96uTlCkjQHtQgfv3Ij9UJCLta5/AXGZ4naRxIkLwtwRqJfyMBhrse7Y1wgP8O+F+XVz58eLH8SxD+uLJyBcFvwW1Pv1q+OrlafhW0FA6/Lb/4FUxgINgRSl18bKOjW1m3CJWaiIdpYc8qSauAbVthBRFXFkSCoEawWbjNa1moTIckcNjBGOPjlwrtDx++Hra/fjvx08x/uTo6SF2cfAvaOx+vvpym0l+ugnxy+u3DefvgS3rXv7D2+QXUCkfvz4fbjwJrS3g4UWSLsL4h5YPUZFjbhE3xzdWEgT3oRVYnAysMu2q5rAbfQSG6VZR0dqzGfEzS/uUfHvupaB+TtLfPg9z+5hSbeuo0yO3nRzgan/7kNxAP/Ni4hryBt9y1Ipgxn4EPQc8WB/czkc/j1p+2nc9pUMQxdsa/7TgO1IayHbSVnOKaO82h9tToynUwyAgDM0zxw1sH28djUygUCoVCoVAoFAqF8i8AkzN/9wN/2qRXWp4NzLaUnJiArDxp6+SI6qRXWp4N1uCZYOSYDsrN41EuzTZFJ2tpSDUlAQlJx9ZgjUcxy1aRqjrYr6kICTFpQhhHFDSkcJbNwWY7qSABb38+FBXFf+5TMpKOnBWQJIlralJwsooqZJO5NV7LOsqaqeUV5CpqVlSyEs44rpEQFdXJxpSswjk6MoqaaSDLUM08yirac5LAzThSPo4QD6lHcl5EljWfVWTEr8nIlEAiTS3CTgZJCSHLca6q+Gbj4iE/jsuLOKJjI0NFnIuyHBJcZJjP6mFa7LbchuTwO4wvQUJCgp3l+LU4sqGQ0B2QQM6KIAG37ShK7EYCOW9L26IMEbEE2rUEPJ/Yfk7lY67IC2t40M/IIVHeVpEtIbj/JloVeMfgxaygDSSQIZ6MBhJAqZBF4rbI44h6IIGZEXIuLyJ1e7apehC87ubJ6J3gukVkbht2AtZcAdl5CdluPoG0DJJdyCA5lMy7rhhIYHCIL7q6IULERNLG1iQYPC8VJZc33PzzqiKuKz/sFvdH7+aHNw0zPxq8VW/yeAxgcJY/Jopr5Kf1aMRThXmiL+VSKBQKhUKhUCgUCoVCiZy//luk/JaMBmeKvvrX/x4p/6NFxBSf0n29ECmvGTEapuiljVqCOBMJYjKKxFX7/X7nOrR+x1jq7lKk7EYjwdyoBDgpPeR1CpV1VC2VPFTthpKgXllfr16HCut3SLAYJUu78lwkjEpQrlQhKbWW16mgbrPWQb1WKAnKA6Vab9+WO4VG16vsvS2jTqvSfDQJFnflWCQwIxLUBtZcbiBQo+QVQimAam8rJWwF8k+e51ULZU8GIfuoVKp7wxK8jJJHkqAVJAVBduh0Oq1Wq94th5DAq3otolap0fQzQrPXAQlqI7F2l0cTsRws49u+G8HftivPR8KoBHAPew34hnwAt7bmFaqF9UIVhcF7S+LVKz0sQa1Rb02QIFJ2+UgUiM+N1wjVfQ+1Gv4wYKu3Dumofz/9tfJ6F6vmNb1eCRVa9V7fm2AFK5Gyy8cjIZYcS0qphGo/1ZpNyMTeHg920AhhBc1Sv4dzPaS740Hh2JI7pVoL1UbV230RKY8kASSFh3zc63QgP9fhJja7TRQV59FKcITkSIhH0jQKx/u//0eE/P1c4CLh1gQIFArlcfnt9d38Q5kdUywLXv/5bv6kqTNjis/23ecvOBMi8n/8AFOsFO/xF4AEc5H0/n+AyRKEfX78Br5/04Qqe5Pj3OcyOWvPSoIxfwEq96Hnz3ffNupovUL8JiEl6Oz7/QEP1CvUg5WxBw7v8RcsfW7HonGAPJzRnmKzgvuI0NNpFqB7VO6isM3jcqVLJOjs7TVLhULJKzRAk25nb7ijdJ/L5HN7Ppre/+/1F0D/ACTArp79KnGZNMIp4L31Ojix8k84D0BnmccGhUpdb9gQ7nGZLH4TZyXBmL8AEQk6/Wp9r97qlmqdZrMfxnVW6qFuy7cC6GZhl0mvVHqIv+Dlt3Y8kt7/DzBJAr7X7+2vo3LZa1QbXojOsleoNAoFkt5qqYMlaFW85kQJVvDfyu1vkAA0uHuJk+WeCA+OeB153GWCJcCJ2sMW3auFdJkApCzwWvVuFzU6zValXqo8wGVCJJgRsQkSNGvNRo/oAH/lwh1V3Bhl7DnnW92WjNZ7TdTq1ZuoOepNv8dlsvJPUZ6ZBGMN5CoUhdVeh/hM1+Gz3gtnBGG4z2UCEsyKabpMTj+9uZto3B8/5DJ5Tm+BUSj/CmhW4iliTfH5AkedXZl3H8r0JNBm5xW5l0gqRV7m+e/PqaiJM3IJ3M9kCUTsM5FxosI5T3IZPXMzu2oydpcEcwwzFyzMyDJ39xI64g+eddxlQuDNTFFDnK5zSDBv756I6P/XPCTgaTo5GcXxShzFh20DJHiKjPcUMYqJRCNuCpyEzJCvTM4FCpi6rpmZjCRm7KKDJKk43P7WxBm5BO5nkgS6ihdTFCRRD6cAMosm1oB38YS1GfLWPWcjadQ9rc3MLXIft/wFGANSIyU121Yt1dFDVRkyfu8c4XeNrTgCi+At3TbR2PwmIMFT5NYjFkARrB+uXhQZHcxAD/f2rFD0s31CwhJAXgIruCXBrPrD9zJJAknxLQElVMEOVRxYOYdMQzuX0Mwc0k1Hy4ARIXP05Boz69ROZNxl4t9QxcQ1gZjBM/CGmUhbTDq+UKoDN35O45CaFDlSPTx9CeYnjSmKSbJVBEtgtOhmFAEJZuYauof5KbpMInoqJHqmJwGFQkF48i/pSTLFRywcXPQIws0yEpi0cGProSJ/96yjkafvMmHw6zS3FmbycjvigyKHihjRWymIF5gwEszAIfJ9JktAEhTDUzDd4f0YQy3axs2JlDtaUwN/ATOp0z53x77xHd+N8OCzTpIgBp19BWk6dJqhrxMGIYZn8MJAO4PPqAwS8f+tEpE43Lp+Ri6TnIXixpwpCCayQ0+y5BD/ki6ZmpSxE6KegH6TqWeGB6uekcskAzfQVkECCTqKIWcaEw3fZYI/fZcJHCuNeuifncuEy2Q4k0voof7pVDzwkKkZXcT+AsbO6M/ZZUKsQEM83EhRD5UXZP06sYpJXCbS83aZWLnAZSJxIV0mpmHa2LvE2IruQBGQU4s5/Tm7TMSiRHxAAhTyek4P8X+nRFEQBBxPUAXcSBChYyyP1QdPVoKJLhNZJcY/Pwer3MOfRL2LJ1opTvMZZBFa5U+S6UlAoUAxmQrPNP2sU/zvo//4U3j+d5rPmUxPgtf/GZr/+22KpeEUXSYPmMjj8rfpvaERVaVYvWPmimHCT+QBEszPSoJqk7xRQj7r5COUAF53v3IT6t7x7PoD5jK5TM/PqGnUanT2W6i+3yn0UKvfr6FmuBdz6rVy8AR7reZVC606KrdqPKp75WE1HjCRx1k6Pi0FxvwFcMG1Ciq1kLfv4bdSUCXc/A34zRz8ye/1OuVeodSrdlv9LioV+sNzYDxMgqk5DG75CzodVKkhtFfF7ybVSyhEHh+SYB8n2Z/Rpnz7fYTwE5WABPPzsRj05keW2P1L6MgjEcclKO97qLnfKe2v1xuN9f56v1RBYQgyQr1SqGMJ1hv9yu+ZyOMqHdFEJQ93mazv4/JvvVndxzmgXqpWUD9UiVgeKNXq45f0el3yetaPTuSx/Co9vebhqATVPWzI0GBsklfT+vX10BKQCW28Rnevhkp73fp+qQKHjknwYuXFC0ggLP73C7LuB0a/pynBqMukst9ovPValf5bnJ3rcGchI4RrQvuxPDKRkccjrwpbxl/VDD9Jx6s0P7W3UkZdJjwgX7dzSArCvZkUivNXoflnOqpp7Kb1DHI4DtPhaU9PAeFZ/XMiCoVCoVAoFAqFQqFQKBTKc4L/o3FbgamNAj8Rwj2fT/lj8P9hJ4AO4DmiKwAAAABJRU5ErkJggg==" alt=""></img><br></br>
                <b className="ml-4">Brand</b>
                {console.log({ props })}
                <div className="mr-5 ml-4">
                    <ul><input type="checkbox"className="mr-1" />Samsung<br></br>
                        <input type="checkbox" className="mr-1"/>Redmi<br></br>
                        <input type="checkbox"className="mr-1" />Nokia<br></br>
                        <input type="checkbox" className="mr-1"/>Vivo<br></br>
                        <input type="checkbox" className="mr-1"/>Oneplus<br></br>
                        <input type="checkbox" className="mr-1"/>OPPO<br></br>
                    </ul>
                </div>
                <b className="ml-4 mb-3">Price</b>
                <div className="ml-4">
                    <a href="" style={{ textDecoration: 'none', color: 'black' }}>Under <i className="fa fa-inr ml-2" aria-hidden="true"></i>10000</a><br></br>
                    <a href="" style={{ textDecoration: 'none', color: 'black' }}><i className="fa fa-inr " aria-hidden="true"></i>1000 - <i className="fa fa-inr ml-2" aria-hidden="true"></i>5000</a><br></br>
                    <a href="" style={{ textDecoration: 'none', color: 'black' }}><i className="fa fa-inr " aria-hidden="true"></i>5000 - <i className="fa fa-inr ml-2" aria-hidden="true"></i>10000</a><br></br>
                    <a href="" style={{ textDecoration: 'none', color: 'black' }}><i className="fa fa-inr " aria-hidden="true"></i>10000 - <i className="fa fa-inr ml-2" aria-hidden="true"></i>20000</a><br></br>
                    <a href="" style={{ textDecoration: 'none', color: 'black' }}>Over <i className="fa fa-inr " aria-hidden="true"></i>20000</a><br></br>
                    <input type="text" className="form-control  mt-2 ml-3 positionForPrice" placeholder="Min"{...<i className="fa fa-inr " aria-hidden="true"></i>}></input>
                    <input type="text" className="form-control positionForPrice mt-1  ml-3 " placeholder="Max"{...<i className="fa fa-inr " aria-hidden="true"></i>}></input>
                    <button type="button" className="btn priceInputBox ml-3 mb-2 mt-2" value="Go" style={{ position: 'absolute' }}>Go</button><br></br>
                </div>
                <div className="mt-4">
                    <b className="ml-4">Deals</b>
                    <div className="ml-4 mt-2">
                        <input type="checkbox" className="mr-1"/>Today&rsquo;s Deals
                    </div>
                </div>
                <div className="mt-4">
                    <b className="ml-4">Made For Amazon</b>
                    <div className="ml-4 mt-2">
                        <input type="checkbox" className="mr-1"/>Made For Amazon
                    </div>
                </div>
                <div className="mt-4">
                    <b className="mt-4 ml-4">Mobile Phone Operating System</b>
                    <div className="ml-4 mt-2">
                        <input type="checkbox"className="mr-1" />Android
                     </div>
                </div>
                <div className="mt-4">
                    <b className="pt-4 ml-4 ">Pay On Delivery</b>
                    <div className="ml-4 mt-2">
                        <input type="checkbox" className="mr-1"/>Eligible for pay on delivery
                    </div>
                </div>
                <div className="mt-4 ml-4">
                    <b>Internal Memory</b>
                    <div className="mt-2">
                    <input type="checkbox" className="mr-1"/>128 GB
                    </div>
                </div>
                <div className="mt-4 ml-4">
                    <b>RAM</b>
                    <div className="mt-2">
                    <input type="checkbox" className="mr-1"/>8Gb & Above<br></br>
                    <input type="checkbox" className="mr-1"/>6Gb
                    </div>
                </div>
                <div className="mt-4 ml-4">
                    <b>Number Of Cores</b>
                    <div className=" mt-2">
                        <input type="checkbox" className="mr-1"/>Octa Core
                    </div>
                </div>
                <div className="mt-4 ml-4">
                    <b>Screen Size</b>
                    <div className=" mt-2">
                        <input type="checkbox" className="mr-1"/>Below 3.9 Inches<br></br>
                        <input type="checkbox" className="mr-1"/>4 to 4.4 Inches<br></br>
                        <input type="checkbox"className="mr-1" />4.5 to 4.9 Inches<br></br>
                        <input type="checkbox"className="mr-1" />5 to 5.4 Inches<br></br>
                        <input type="checkbox"className="mr-1" />5.5 Inches & above<br></br>
                    </div>
                </div>
                <div className="mt-4 ml-4">
                    <b>Processor Speed</b>
                    <div className=" mt-2">
                        <input type="checkbox" className="mr-1" />2 - 2.4 GHz
                    </div>
                </div>
                <div className="mt-4 ml-4">
                    <b>Color</b>
                    <div className=" mt-2">
                        <button className="btn btn-danger mr-2" style={{ height: '30px', width: '30px' }} value="btn"></button>
                        <button className="btn btn-primary mr-2" style={{ height: '30px', width: '30px' }} value="btn"></button>
                    </div>
                </div>
                <div className="mt-4 ml-4">
                    <b>Front Camera Resolution</b>
                    <div className=" mt-2">
                        <input type="checkbox" className="mr-1"/>Up to 3.9 MP <br></br>
                        <input type="checkbox" className="mr-1"/>4 - 7.9 MP<br></br>
                        <input type="checkbox"className="mr-1" />8 - 11.9 MP<br></br>
                        <input type="checkbox"className="mr-1" />12 - 15.9 MP<br></br>
                        <input type="checkbox"className="mr-1" />16 - 19.9 MP<br></br>
                        <input type="checkbox"className="mr-1" />20 - 23.3 MP<br></br>
                        <input type="checkbox"className="mr-1" />24 - 27.9 MP<br></br>
                        <input type="checkbox"className="mr-1" />28 - 31.9 MP<br></br>
                    </div>
                </div>
            </div>
        </div>
  )
}
