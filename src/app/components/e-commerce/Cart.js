import React from 'react'
import { Accordion, AccordionItem, AccordionItemButton, AccordionItemHeading, AccordionItemPanel } from 'react-accessible-accordion'
import NumberFormat from 'react-number-format'
import { Link } from 'react-router-dom'
import CartDetails from '../../../data/CartData'
import SaveforLater from '../../../data/SaveforLater'
import Sider from './Sider'

export default function Cart () {
    <Sider name='mathi' />
    //   const [Subtotal, setSubtotal] = useState(0)
    const showPrice = (total) => {
      return (
        <NumberFormat value={total} displayType={'text'} thousandSeparator={true} prefix={'Rs.'} />
      )
    }
    //   const savedItems = []
    //   const addToSavedItems = (Product) => {
    //     savedItems.push(Product)
    //   }
    const showEmiAvailableInCart = () => {
      return (
        <Accordion allowMultipleExpanded>
            <AccordionItem>
                <AccordionItemHeading>
                    <AccordionItemButton className=" card mt-4">
                        EMI Available<i className="fa fa-chevron-down ml-1" aria-hidden="true"></i>
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel>
                    <p className="card text-left emiAvilableInCart">
                    Your order qualifies for EMI with valid credit cards (not available on purchase of Gold, Jewelry, Gift cards and Amazon pay balance top up).
                    </p>
                </AccordionItemPanel>
            </AccordionItem>
        </Accordion>
      )
    }
    const totalPrice = CartDetails.reduce((accum, item) => accum + item.price, 0)
    return (
        <div className="row">
            <div className="col-md-8 text-left pl-4">
                <strong>Pay faster for all your shopping needs with <a style={{ textDecoration: 'none', color: '#b22222' }} href="">Tekafforde Pay balance</a></strong><br></br>
                <span>Get Instant refund on cancellations | Zero payment failures</span>
                <div className="card mt-5 ml-1">
                    <div className="card-title">
                        <h4 className="ml-2 mb-0 mt-3"><b>Shopping Cart</b></h4>
                        <span className="ml-2">Deselect all items</span>
                    </div>
                    <span className="cartPrice">price</span>
                    <hr></hr>
                    {CartDetails.map((cart) => (
                        <div className="row card-body" key={cart}>
                            {/* {setItemNoToBePushedInSavedItems(itemNoToBePushedInSavedItems + 1)} */}
                            <div key={cart} className="col-md-1">
                                <input type="checkbox"></input>
                            </div>
                            {/* {setSubtotal(cart.price)} */}
                            <div className="col-md-1 ml-2 mt-0">
                                <img src={cart.images.imgUrl} height="100" width="90" alt=""></img>
                            </div>
                            <div className="col-md-9 text-left" style={{ float: 'right' }}>
                                <div style={{ float: 'right' }} className="ml-5">
                                    <i className="fa fa-inr ml-1 mr-2" aria-hidden="true"></i> <b>{cart.price}</b>
                                </div>
                                <h5 className="ml-4">{cart.productName}</h5>
                                <span className="ml-4">In stock</span><br></br>
                                <span className="ml-4">Eligible for FREE shipping</span><br></br>
                                <img src="images\TAvertical.png" className="ml-4" alt="" height="25" width="90"></img><br></br>
                                <input type="checkbox" className="ml-4 mt-2 mr-2" /><small>This will be a gift</small><a href="" style={{ textDecoration: 'none' }}><small className="ml-2">learn more</small></a>
                                <div className="row">
                                    <div className="col-md-4">
                                        <label className="mr-1 ml-4 mt-2">Quantity</label>
                                        <select style={{ width: 50 }} placeholder="Qty">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                        </select>
                                    </div>
                                    <div className="col-md-8 cartPageOptions mt-2">
                                        &nbsp;Delete&nbsp;&nbsp;|&nbsp;&nbsp;<a type="button" href="">Save for later</a>&nbsp;&nbsp;|&nbsp;&nbsp;see more like this&nbsp;&nbsp;
                                    </div>
                                </div>
                                <hr></hr>
                            </div>
                        </div>
                    ))}
                    <div className="card-footer">
                        <h5 className="subTotalText">Subtotal&nbsp;(&nbsp;{CartDetails.length} Items&nbsp;){showPrice(totalPrice)}</h5>
                    </div>
                </div>
                <div className="card mt-4  ml-1 mb-5">
                    <div className="card-title">
                        <h4 className="ml-4 mt-2"><b>Your Items</b></h4>
                        <hr></hr>
                    </div>
                    <div className="card-body row">
                        {SaveforLater.map((items) =>
                            <div key={items} className=" card ml-4 col-md-3 mb-3">
                                <img src={items.images.imgUrl} className="mt-4"></img>
                                <b className="text-center mt-2"><a href="" style={{ textDecoration: 'none' }}>{items.productName}</a></b>
                                <span style={{ display: 'inline' }}><i className="fa fa-inr ml-1 mr-2" aria-hidden="true"></i><strong>{items.price}</strong></span>
                                <small>In Stock</small>
                                <small>Eligible for FREE shipping</small>
                                <img src="images\TAvertical.png" className="mt-2" alt="" height="20" width="100"></img>
                                <input className="form-control  text-center  mt-2" value="Add to Cart" type="button"></input>
                                <a href="" style={{ textDecoration: 'none' }} className="mt-3">Delete</a>
                                <a href="" style={{ textDecoration: 'none' }}>Move to cart</a>
                                <a href="" style={{ textDecoration: 'none' }}>See more like this</a>
                            </div>
                        )}
                    </div>
                </div>
            </div>
            <div className="col-md-3 mt-5">
                {/* <div className="card mt-5 mb-4">
                    <img src="https://images-eu.ssl-images-amazon.com/images/G/31/checkout/assets/TM_desktop._CB443006202_.png"></img>
                </div> */}
                <div className="card">
                    <div classNam="card-header text-center" style={{ backgroundColor: '#f4f5db' }}>
                        <i className="fa fa-check mr-2" aria-hidden="true"></i>
                        <small>Your order is eligible for FREE Delivery. Select this option at checkout. Details</small>
                    </div>
                    <div className="card-body">
                        <strong>Subtotal({CartDetails.length}&nbsp;&nbsp;Items):&nbsp;<i className="fa fa-inr ml-1 mr-2" aria-hidden="true"></i>{showPrice(totalPrice)}
                        </strong><br></br>
                        <input type="checkbox" className="mr-2" />This order contains gift
                        <Link to="cart" style={{ textDecoration: 'none', color: 'white' }}><input className="form-control addToCartBtn text-center ml-2 mt-3" value="Proced to checkout" type="button"></input></Link>
                        {showEmiAvailableInCart()}
                    </div>
                </div>
            </div>
        </div>
    )
}
